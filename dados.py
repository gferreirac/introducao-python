entrada = ''
saida = ''
dados = {}

for key1 in ['A', 'T', 'C', 'G']:
    for key2 in ['A', 'T', 'C', 'G']:
        dados[key1+key2] = 0
        
for e in range(2): 
    if e == 0:
        entrada = open("assets/bacteria.fasta").read()
        saida = open("bacteria.html","w")
    else:
        entrada = open("assets/human.fasta").read()
        saida = open("human.html","w")
        
    entrada = entrada.replace("\n","")

    for i in range(len(entrada)-1):
        dados[entrada[i]+entrada[i+1]] += 1      
        
    #html    
    i = 1
    for k in dados:
        transparencia = dados[k]/max(dados.values())
        saida.write(
            "<div style='width:100px; border:1px solid #111; height:100px; float:left; color:#fff; background-color:rgba(0,0,0,"+str(transparencia)+")'>"+k+"</div> "
        )
        if i%4 == 0:
            saida.write("<div style='clear:both'></div>")
        i+=1
        
    saida.close()
     