import matplotlib.pyplot as plt

x = [1,2,5]
y = [2,3,1]

titulo = "Gráfico de Linhas"
eixox = "Eixo X"
eixoy = "Eixo Y"
plt.title(titulo)
plt.xlabel(eixox)
plt.ylabel(eixoy)

plt.plot(x, y)
plt.show()