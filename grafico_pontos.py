import matplotlib.pyplot as plt

x = [1,2,5,6,3]
y = [2,3,1,9,6]

titulo = "Gráfico de pontos"
eixox = "Eixo X"
eixoy = "Eixo Y"
plt.title(titulo)
plt.xlabel(eixox)
plt.ylabel(eixoy)

plt.plot(x, y, color="k", linestyle="--")
plt.scatter(x, y, label = "meus pontos", color = "k", marker=".", s=200)
plt.legend()
plt.show()