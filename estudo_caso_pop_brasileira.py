import matplotlib.pyplot as plt

dados = open("assets/populacao_brasileira.csv").readlines()

x=[]
y=[]

for i in range(len(dados)):
    if i != 0 :
        linha = dados[i].split(";")
        x.append(int(linha[0]))
        y.append(int(linha[1]))
        
titulo = "Crescimento da População Brasileira 1980-2016"
legendax = "Ano"
legenday = "População x100.000.000"

plt.title(titulo)
plt.xlabel(legendax)
plt.ylabel(legenday)

plt.bar(x,y, color="#ccc")
plt.plot(x,y, color="k", linestyle="--")

plt.savefig("assets/populacao_brasileira.pdf")
plt.show()